#!/bin/bash

activate_tdd_venv () {
    echo "activating venv..."
    source ~/dev/tdd/tddvenv/bin/activate
}

start_server () {
    python ~/dev/tdd/manage.py runserver
}

activate_tdd_venv
start_server