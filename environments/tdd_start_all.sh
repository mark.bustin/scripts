#!/bin/bash

echo "starting setup..."

activate_tdd_venv () {
    echo "activating venv..."
    source ~/dev/tdd/tddvenv/bin/activate
}

start_vs_code () {
    echo "loading VS Code..."
    cd ~/dev/tdd
    code .
}

start_tdd_server () {
    echo "starting server..."
    gnome-terminal --tab -- bash -c 'source ~/dev/scripts/environments/tdd_start_server.sh'
}

activate_tdd_venv
start_vs_code
start_tdd_server

echo "finished!"
